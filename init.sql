-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `email` varchar(128) NOT NULL COMMENT 'E-Mail',
  `nome` varchar(255) NOT NULL COMMENT 'Nome',
  `senha` varchar(128) NOT NULL COMMENT 'Senha',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2019-10-20 17:02:19
